**SDK Methods**

- ***`getSupportedDevices()`***
    - **Description:** Returns a list of device type/connectors that are supported by the SDK. Every device will include if is discoverable a the list of supported variables.
    - **Parameters:** None
    - **Return value:** a list of dictionaries with the following fields:
        - `name`: String with the name of the device type.
        - `discoverable`: Boolean indicating if the device is discoverable as bluetooth.
        - `variables`: List of strings with the supported variables for the device.
    - **Exceptions raised:** None.
    
    ```
    [{
        "name": "beatone",
        "discoverable": true,
        "variables": [
            "hr",
            "steps",
            "distance",
            "calories"
        ]
    },
    {
        "name": "googlefit",
        "discoverable": false,
        "variables": [
            "hr",
            "steps",
            "distance",
            "calories",
            "hrv",
            "sleep"
        ]
    }]
    ```
    
- ***`discover(deviceType, callback)`***
    - **Description:** Launches a process to discover devices of the selected type. Using the callback function this method will send discovered devices in an asynchronous way.
    - **Parameters:**
        - `deviceType` (mandatory): String with the name of supported device.
        - `callback(device)` (optional): A callback function for receiving detected devices.
            
            ```
            {
                "deviceType": "beatone",
                "uuid": "uuid",
                "name": "display name"
            }
            ```
            
    - **Return value:** None.
    - **Exceptions raised:** None.
- ***`stopDiscover()`***
    - **Description:** Stops the process of discovering devices.
    - **Parameters:** None.
    - **Return value:** None.
    - **Exceptions raised:** None.
- ***`pair(deviceType, variables, deviceId?)`***
    - **Description:** Depending on the deviceType, this functionality will proceed differently.
        - Beat-One: Try to connect to device received as a parameter
            - Timeout if not devices are detected
            - When finished, stores the reference to the selected device
        - GoogleFit/AppleHealthKit
            - Proceed to configure permissions and set variables to read.
        - More than one device per device Type is not allowed.
    - **Parameters:**
        - `deviceType` (mandatory): String with the name of supported device type. Obtained using ---`getSupportedDevices()`
        - `variables` (mandatory): A list of strings with the variables to get from the device.
        - `deviceId` (optional): The device UUID to connect.
    - **Return value:** None.
    - **Exceptions raised:** None.
- ***`unpair(deviceType)`***
    - **Description:** Depending on the deviceType, it will remove the device from the paired list.
    - **Parameters:**
        - `deviceType` (mandatory): String with the name of supported device.
    - **Return value:** None.
    - **Exceptions raised:** None.
- ***`getPairedDevices()`***
    - **Description:** Returns a list of paired devices and status, deviceType, deviceId, including last synchronization timestamp.
    - **Parameters:** None.
    - **Return value:** a list of dictionaries with the following fields:
        - `device`: Dictionary containing the device information.
            - `deviceType`: String with the name of the device type.
            - `uuid`: String with the unique identifier of the device.
            - `name`: String with the display name of the device.
        - `variables`: List of dictionaries with the following fields:
            - `name`: String with the name of the variable.
            - `lastSynchronization`: Timestamp with the last synchronization date of the variable.
    - **Exceptions raised:** None.
    
    ```
    [{
        "device":{
            "deviceType": "beatone",
            "uuid": "uuid",
            "name":"display name"
        },
        "variables": [
            {
                "name": "hr",
                "lastSynchronization": 13233434343
            },
            {
                "name": "steps",
                "lastSynchronization": 13233434343
            }
        ]
    }]
    ```
    
- ***`getDeviceBattery(deviceId)`***
    - **Description:** Returns the battery percentage of the device. Only makes sense for wearables (like Beat-One, not connectors like GoogleFit).
    - **Parameters:**
        - `deviceId` (mandatory): String with the id of paired device.
    - **Return value:** An `int` number with % of battery.
    - **Exceptions raised:** None.
- ***`synchronize()`***
    - **Description:** Performs a data collection (all available variables) from all paired devices to the local HiveDB. Need to ask for data from the last synchronization date.
    - **Parameters:** None.
    - **Return value:** None.
    - **Exceptions raised:** None.
- ***`query(variable, from?, to?)`***
    - **Description:** Extracts data for a concrete variable from local database.
    - **Parameters:**
        - `variable` (mandatory): String with the name of the variable.
        - `from` (optional): Timestamp with the starting date for data.
        - `to` (optional): Timestamp with the ending date for data.
    - **Return value:** a list of dictionaries with the following fields:
        - `device`: Dictionary containing the device information.
            - `deviceType`: String with the name of the device type.
            - `uuid`: String with the unique identifier of the device.
            - `name`: String with the display name of the device.
        - `timestamp`: Timestamp with the data timestamp.
        - `type`: String with the type of variable.
        - `value`: String with the value of the variable.
        - `value2`: String with additional value of the variable.
    - **Exceptions raised:** None.
    
    ```
    [{
        "device":{
            "deviceType": "beatone",
            "uuid": "uuid",
            "name":"display name"
        },
        "timestamp":121212121,
        "type":"hr",
        "value":"65",
        "value2":""
    }]
    
    ```
    
- **`clearDb`**

*** Sdk Status and error


# Discovery

The sdk discover function has now another optional parameter called onDiscoveredError used, basically to return if the bluetooth is disabled and the discovery cannot be executed.


```
await sdk.discover(SupportedDevices.BEATONE.name,
        onDiscoveredDevices: (devices) => {
              if (mounted)
                {
                  setState(() {
                    discoveredDevices.clear();
                    discoveredDevices.addAll(devices);
                  })
                }
            },
        onDiscoveredError: (error) => {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(error.toString()),
              ))
            });
```


# Connection Status

When pairDevice is execute this performs a connection with the device, depending on the device type we can get different status when when we are listening for changes in the connection.

First we perform the pairing via SDK pairing method:

```sdk.pairDevice(device.name, device.variables, device);```

After that we need to listen for connection changes, so we subscribe to the sdk connection stream:


```void listenConnectionChanges(String name) {
    sdk.connection.listen((event) {
      if (event.status == ConnectionStatusValues.CONNECTED) {
        if (mounted) {
          setState(() {
            isConnecting = false;
          });
        }
        openConnectedDevicePage(name);
      } else if (event.status ==
          ConnectionStatusValues.GOOGLE_FIT_NOT_GRANTED) {
        if (mounted) {
          setState(() {
            isConnecting = false;
          });
        }
        const snackBar = SnackBar(
          content: Text('The user has not authorized the use of Google Fit'),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      } else if (event.status == ConnectionStatusValues.CONNECTION_ERROR ||
          event.status == ConnectionStatusValues.CONNECTION_FAILED) {
        cancelConnection();
        const snackBar = SnackBar(
          content: Text(
              'There was an error while trying to connect with the device'),
        );
        if (mounted) {
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      } else if (event.status ==
          ConnectionStatusValues.DEVICE_TYPE_CURRENTLY_PAIRED) {
        if (mounted) {
          cancelConnection();
          var snackBar = SnackBar(
            content: Text('There is already a paired $name type device'),
          );
          if (mounted) {
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        }
      }
    });
  }

```



The possible events that we can receive are found in the **ConnectionStatusValues** enum, inside the SDK. 


```
enum ConnectionStatusValues {
  CONNECTION_FAILED,
  GOOGLE_FIT_NOT_GRANTED,
  DISCONNECTED,
  CONNECTED,
  CONNECTION_ERROR,
  DEVICE_TYPE_CURRENTLY_PAIRED
}
```

Detailed status are shown in the following table:

|STATUS|DESCRIPTION|BEATONE|GOOGLE FIT|
| ----------- | ----------- | ----------- | ----------- |
|CONNECTION_FAILED|The device has been disconnected or the connection to the device could not be established|:heavy_check_mark:||
|GOOGLE_FIT_NOT_GRANTED|This error occurs when we do not authorize the application to use our Google Fit account ||:heavy_check_mark:|
|DISCONNECTED|Devices has been disconnected for some reason and the device is saved as disconnected in the paired devices list. Also used to initialize paired devices list with this status|:heavy_check_mark:|:heavy_check_mark:|
|CONNECTED|After a Beatone is connected or a Google Fit is granted and connected|:heavy_check_mark:|:heavy_check_mark:|
|CONNECTION_ERROR|The bluetooth is disabled when we had a BeatOne type device connected |:heavy_check_mark:||
|DEVICE_TYPE_CURRENTLY_PAIRED|Since it is only allowed to pair one device for each of the SDK supported  types, if we try to connect a device of a type that we had previously paired we will get this error|:heavy_check_mark:|:heavy_check_mark:|


# Synchronization

The synchronization result is managed with the enum

`enum SynchronizationStatus { SUCCESS, FAIL }`

So, to know when the synchronization has finished and it's result, the SDK's synchronize method has an optional callback parameter named **onSynchronizationFinished** that we can use to know the synchronization result. This result is a Map with the device type and it's synchronization result.When the synchronization has finished successfully we will obtain a result with SUCCESS value and FAIL otherwise.


```
sdk.synchronize(
        onSynchronizationFinished: (result) => {
              setState(() {
                synchronizing = false;
                var syncResult = "";
                result.forEach((key, value) {
                  syncResult = "$syncResult$key:$value\n";
                });
                var snackBar = SnackBar(
                  content:
                      Text('Synchronization finished with result $syncResult'),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              })
            });
```



